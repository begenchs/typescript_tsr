import { useContext, useRef } from 'react';
import styled from 'styled-components';
import { GlobalContext } from './GlobalState';
import { useClickOutside } from './useClickOutside';

export const ReducerButton = () => {
  const ref = useRef<HTMLDivElement>(null!);
  const context = useContext(GlobalContext);

  const { rValue, turnOn, turnOff } = context;

  useClickOutside(ref, () => {
    console.log('clicked outside');
  });

  return (
    <Wrapper ref={ref}>
      {rValue && <h1>Visible</h1>}
      <button onClick={turnOn}>Action One</button>
      <button onClick={turnOff}>Action Two</button>
      {/* <button onClick={() => dispatch({ type: 'tow' })}>Action Two</button> */}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background: red;
`;
