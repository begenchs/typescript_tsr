import { Component } from 'react';

type Props = {
  title: string;
};

export default class BigC extends Component<Props> {
  render() {
    return (
      <div>
        <h1>I am in a class component</h1>
      </div>
    );
  }
}
