// type User = {
//   name: string;
// };

type Props = {
  title: string;
  isActive: boolean;
  // thing: number;
  // thing2: string[];
  // func: () => void;
  // user: User;
};

export const Head = ({ title, isActive }: Props) => {
  return (
    <div>
      <h1>{title}</h1>
      {isActive && <h3>Active</h3>}
    </div>
  );
};
