import React, { PropsWithChildren } from 'react';

type Props = {
  // onClick(text: string): void;
  // onClick: (e: React.MouseEvent) => void; // Basic Mouse Event
  // onChange: (e: React.FormEvent<HTMLInputElement>) => void; // Basic input event
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

export const Button: React.FC<PropsWithChildren<Props>> = ({ onClick, children }) => {
  return <button onClick={onClick}>{children}</button>;
};
