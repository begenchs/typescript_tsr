import { useRef, useState } from 'react';

export const Input = () => {
  // const [name, setName] = useState<string>('');
  // const [name, setName] = useState<string | null>('');
  const [name, setName] = useState('');
  // !null is Read Only
  const ref = useRef<HTMLInputElement>(null);

  console.log('ref', ref?.current?.value);

  return <input ref={ref} value={name} onChange={(e) => setName(e.target.value)} />;
};
