import { createContext, useReducer } from 'react';

export const initialValues = {
  rValue: true,
  turnOn: () => {},
  turnOff: () => {},
};

export const GlobalContext = createContext(initialValues);

// type State = {
//   rValue: boolean;
// };
interface State {
  rValue: boolean;
}

type Action = {
  type: 'one' | 'two';
};
function reducer(state: State, action: Action) {
  switch (action.type) {
    case 'one':
      return { rValue: true };
    case 'two':
      return { rValue: false };
    default:
      return state;
  }
}

type Props = { children?: React.ReactNode };

export const GlobalProvider: React.FC<Props> = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialValues);

  return (
    <GlobalContext.Provider
      value={{
        rValue: state.rValue,
        turnOn: () => dispatch({ type: 'one' }),
        turnOff: () => dispatch({ type: 'two' }),
      }}>
      {children}
    </GlobalContext.Provider>
  );
};
